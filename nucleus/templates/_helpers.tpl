{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "name" -}}
  {{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "nucleus.fullname" -}}
  {{- printf "%s-%s" .Release.Name "nucleus" | trunc 63 | trimSuffix "-" }}
{{- end }}


{{/*
Override full names of upstream charts for deterministic host names
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "cockroachdb.fullname" -}}
  {{- printf "%s-%s" .Release.Name "db" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "selfcerts.fullname" -}}
  {{- printf "%s-%s" (include "cockroachdb.fullname" .) "selfsigner" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "rotatecerts.fullname" -}}
  {{- printf "%s-%s" (include "cockroachdb.fullname" .) "rotateselfsigner" | trunc 63 | trimSuffix "-" -}}
{{- end -}}


{{- define "nucleus.database.hostname" -}}
  {{- if .Values.global.database.hostname }}
    {{- .Values.global.database.hostname }}
  {{- else }}
    {{- printf "%s-db.%s.svc.cluster.local" .Release.Name .Release.Namespace -}}
  {{- end }}
{{- end }}


{{- define "nucleus.database.url" -}}
  {{- printf "cockroachdb://%s@%s:%s/%s" .Values.global.database.user (include "nucleus.database.hostname" .) (.Values.global.database.port | toString ) .Values.global.database.name -}}
{{- end }}
